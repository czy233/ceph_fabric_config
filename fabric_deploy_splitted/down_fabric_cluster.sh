#!/bin/bash

source /etc/profile

# down fabric network
echo -n "down fabric network ... "
for i in {0..3}
do
    ssh node$i "
        cd $GOPATH/src/fabric-sdk/gen_crypto
        docker-compose -f docker-compose-peer${i}.yaml down -v > /dev/null 2>&1
    " &
done
for i in {1..3}
do
    ssh order$i "
        cd $GOPATH/src/fabric-sdk/gen_crypto
        docker-compose -f docker-compose-order${i}.yaml down -v > /dev/null 2>&1
    " &
done

wait
echo "done"