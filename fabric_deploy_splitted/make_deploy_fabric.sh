#!/bin/bash -e

# down掉正在运行的区块链系统
cd ~/ceph_fabric_config/fabric_deploy_splitted
./down_fabric_cluster.sh

# get fabric codes & make docker images
echo "get fabric source & make docker images codes"
for i in {0..3}
do
    ssh node$i "
        source /etc/profile
        cd ~/go/src
        if [ ! -d "fabric" ]; then
            git clone https://gitee.com/czy233/fabric.git $GOPATH/src/fabric
            cd ./fabric
            git checkout v2.2-demo
        else
            cd ./fabric
            git pull
            git checkout v2.2-demo
        fi
        make clean
        echo y | docker image prune -a
        make release
        make docker
    " &
done 
for i in {1..3}
do
    ssh order$i "
        source /etc/profile
        cd ~/go/src
        if [ ! -d "fabric" ]; then
            git clone https://gitee.com/czy233/fabric.git $GOPATH/src/fabric
            cd ./fabric
            git checkout v2.2-demo
        else
            cd ./fabric
            git pull
            git checkout v2.2-demo
        fi
        make clean
        echo y | docker image prune -a
        make release
        make docker
    " &
done 
wait
echo "get fabric source codes & make docker images done"

# 启动区块链系统
cd ~/ceph_fabric_config/fabric_deploy_splitted
./deploy_fabric_peer_orders.sh