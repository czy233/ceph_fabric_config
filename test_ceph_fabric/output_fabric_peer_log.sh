#/bin/bash -e

peer_name=`docker ps | grep fabric-peer | awk '{print $14}'`
sudo cat `docker inspect --format='{{.LogPath}}' $peer_name` | grep "Committed block" > ./fabric_validate.log