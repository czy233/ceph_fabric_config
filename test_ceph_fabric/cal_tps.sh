# #/bin/bash -e

# peer_name=`docker ps | grep fabric-peer | awk '{print $14}'`
# sudo cat `docker inspect --format='{{.LogPath}}' $peer_name` | grep "with 500 transaction(s)" > ~/fabric_validate.log
# grep "block \[50\]" ~/fabric_validate.log > ~/fabric_validate_res.log
# time1=`sed -n 's/.*"time":"\(.*\)T\(.*\)Z.*/\1 \2/p' ~/fabric_validate_res.log`
# grep "block \[200\]" ~/fabric_validate.log > ~/fabric_validate_res.log
# time2=`sed -n 's/.*"time":"\(.*\)T\(.*\)Z.*/\1 \2/p' ~/fabric_validate_res.log`

# timeStamp1=$(date --date="$time1" +%s.%N)
# timeStamp2=$(date --date="$time2" +%s.%N)
# # echo $timeStamp1
# # echo $timeStamp2

# delta_time=$(echo "$timeStamp2-$timeStamp1" | bc)
# echo "cost time: ${delta_time} s"

# TPS=$(echo "75000/$delta_time" | bc)
# echo "TPS: $TPS"

# rm ~/fabric_validate.log ~/fabric_validate_res.log

#/bin/bash -e

block_start="50"
block_end="550"

peer_name=`docker ps | grep fabric-peer | awk '{print $NF}'`
sudo cat `docker inspect --format='{{.LogPath}}' $peer_name` | grep "with 500 transaction(s)" > ~/fabric_validate.log
grep "block \[$block_start\]" ~/fabric_validate.log > ~/fabric_validate_res.log
time1=`sed -n 's/.*"time":"\(.*\)T\(.*\)Z.*/\1 \2/p' ~/fabric_validate_res.log`
grep "block \[$block_end\]" ~/fabric_validate.log > ~/fabric_validate_res.log
time2=`sed -n 's/.*"time":"\(.*\)T\(.*\)Z.*/\1 \2/p' ~/fabric_validate_res.log`

timeStamp1=$(date --date="$time1" +%s.%N)
timeStamp2=$(date --date="$time2" +%s.%N)
# echo $timeStamp1
# echo $timeStamp2

delta_time=$(echo "$timeStamp2-$timeStamp1" | bc)
echo "average TPS between block [$block_start - $block_end], cost time: ${delta_time}s"

TPS=$(echo "500*($block_end-$block_start)/$delta_time" | bc)
echo -e "TPS: \033[1;36m$TPS\033[0m"

rm ~/fabric_validate.log ~/fabric_validate_res.log
