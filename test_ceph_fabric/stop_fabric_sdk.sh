#/bin/bash -e

source cluster_ip.conf

function stop_fabric_sdk() {
    # stop fabric-sdk for 4 orgs
    for node in ${FABRIC_SDK_IP[@]}
    do
        ssh $node "
            fabric_sdk=\$(ps -ef | grep main | grep -v grep | awk '{print \$2}')
            for id in \$fabric_sdk
            do
                sudo kill \$id
            done
        " &
    done
    wait
}

stop_fabric_sdk