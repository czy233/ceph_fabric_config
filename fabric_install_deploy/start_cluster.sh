#!/bin/bash

USER='mcloud'

# do operation in node0 ?
if [ $(hostname -s) != "node0" ]; then
    echo "please deploy fabric in node0"
    exit -1
fi

echo "start deploy a fabric cluster ... "

echo "start install go & docker ... "
for i in {0..3}
do
    (
        echo "ssh to [node$i] to install go & docker"
        ssh node$i "
            if [ ! -d '/home/$USER/ceph_fabric_config' ]; then
                git clone https://gitee.com/czy233/ceph_fabric_config.git /home/$USER/ceph_fabric_config > /dev/null 2>&1
            else
                cd /home/$USER/ceph_fabric_config
                git pull origin master
            fi
            cd /home/$USER/ceph_fabric_config/fabric_install_deploy
            ./install_go_docker.sh > install_go_docker.log"
    ) &
done
echo "wait install_go_docker finish..."
wait

echo "start install fabric ... "
for i in {0..3}
do
    (
        echo "ssh to [node$i] to install fabric"
        ssh node$i "
            cd /home/$USER/ceph_fabric_config/fabric_install_deploy
            ./install_fabric.sh > install_fabric.log"
    ) &
done
echo "wait install_fabric finish..."
wait


echo "finish deploy a fabric cluster!"