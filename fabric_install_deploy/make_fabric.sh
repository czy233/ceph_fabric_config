#!/bin/bash -e

# down掉正在运行的区块链系统
cd ~/ceph_fabric_config/fabric_install_deploy
./down_fabric_cluster.sh

# get fabric codes & make docker images
echo "get fabric source & make docker images codes"
for i in {0..3}
do
    ssh node$i "
        source /etc/profile
        cd ~/go/src
        if [ ! -d "fabric" ]; then
            git clone https://gitee.com/czy233/fabric.git $GOPATH/src/fabric
            cd ./fabric
            git checkout v2.2-demo
        else
            cd ./fabric
            git checkout v2.2-demo
            git pull
        fi
        make clean
        echo y | docker image prune -a
        make release
        make docker
    " &
done 
wait
echo "get fabric source codes & make docker images done"

# # 将镜像分发到各个节点
# echo "start push images to dockerhub"
# docker tag hyperledger/fabric-peer czy233/fabric-peer
# docker push czy233/fabric-peer

# echo "start pull images for all nodes "
# for i in {1..3}
# do
#     ssh node$i "docker pull czy233/fabric-peer" &
# done
# wait
# echo "done"

# 启动区块链系统
cd ~/ceph_fabric_config/fabric_install_deploy
./deploy_fabric.sh