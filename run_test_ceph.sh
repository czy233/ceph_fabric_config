# 调用s3 API 测试ceph(重新编译osd模块并安装ceph)
if [ $1 && $1 == 'rebuild osd' ]; then
	# 调用s3 API 测试ceph(重新编译osd模块并安装ceph)
	cd ~/ceph-invoke-fabric/build
	../src/stop.sh
	rm -rf out dev
	git pull origin master
	sudo make osd -j32
	sudo make install -j32
	RGW=1 ../src/vstart.sh --debug --new -X --localhost --bluestore
	./bin/ceph -s
	rm ~/ceph_fabric_config/secret.json
	sudo ./bin/radosgw-admin user create --uid="testuser" --display-name="First User" >> ~/ceph_fabric_config/secret.json
	python3 ~/ceph_fabric_config/save_to_s3.py
	ll -h ~/ceph-invoke-fabric/build/out/osd.*.log
	cp ~/ceph-invoke-fabric/build/out/osd.*.log /tmp
	./bin/ceph osd map default.rgw.buckets.data test
else
	# 调用s3 API 测试ceph(重新启动ceph)
	cd ~/ceph-invoke-fabric/build
	../src/stop.sh
	rm -rf out dev
	MON=1 MDS=1 MGR=1 ../src/vstart.sh --new -X --localhost --without-dashboard
	./bin/ceph -s
	rm ~/ceph_fabric_config/secret.json
	sudo ./bin/radosgw-admin user create --uid="testuser" --display-name="First User" >> ~/ceph_fabric_config/secret.json
	python3 ~/ceph_fabric_config/save_to_s3.py
	ll -h ~/ceph-invoke-fabric/build/out/osd.*.log
	cp ~/ceph-invoke-fabric/build/out/osd.*.log /tmp
	./bin/ceph osd map default.rgw.buckets.data test
fi
