#!/bin/bash -e
# auto test fabric performance using diff go routine number
# range: 400 - 6400, step size: 400

FABRIC_BRANCH='split_Peer_Order'

# re-deploy fabric envs
function redeploy_fabric() {
    ssh 10.10.9.40 "
        cd $HOME/ceph_fabric_config/fabric_deploy_splitted
        git pull > /dev/null 2>&1
        ./deploy_fabric_peer_orders.sh > /dev/null 2>&1
    "
}

cd $HOME/ceph_fabric_config/fabric_bench
git pull -q

# update fabric-sdk
cd $HOME/go/src/fabric-sdk
git pull -q
git checkout $FABRIC_BRANCH

goNum=400

rm ~/test_result.txt

for i in {1..16} 
do
    for j in {1..5} 
    do
        echo -n "goNum $goNum, loop $j -->> "
        redeploy_fabric
        go run benchmark.go -t $goNum
    done
    echo "" >> ~/test_result.txt
    let goNum=$goNum+400
done
