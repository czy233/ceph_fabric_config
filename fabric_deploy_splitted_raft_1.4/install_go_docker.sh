#!/bin/bash

# install Golang
if ! type go >/dev/null 2>&1; then
	# 1. download Golang
	cd $HOME
	wget https://studygolang.com/dl/golang/go1.16.10.linux-amd64.tar.gz --no-check-certificate > /dev/null 2>&1
	sudo tar -xzf go1.16.10.linux-amd64.tar.gz -C /usr/local/> /dev/null 2>&1
	rm go1.16.10.linux-amd64.tar.gz

	# 2. add env PATH
	echo "export GOROOT=/usr/local/go" | sudo tee -a /etc/profile
	echo "export GOPATH=$HOME/go" | sudo tee -a /etc/profile
	echo "export PATH=/usr/local/go/bin:$PATH" | sudo tee -a /etc/profile
	source /etc/profile
else
	echo "go has been installed"
fi
# 3. change go agent
go env -w GOPROXY=https://goproxy.io,direct
if [ $? -ne 0 ]; then
	echo "change go proxy error"
	exit -1
fi
# should turn off GO111MODULE for fabric v1.4
go env -w GO111MODULE=off
if [ $? -ne 0 ]; then
	echo "set GO111MODULE error"
	exit -1
fi
# 4. create go dir
if [ ! -d "$HOME/go/src" ]; then
	mkdir -p $HOME/go/src
fi
# sudo chmod -R 775 go

# install Docker
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
if [ $? -ne 0 ]; then
	echo "install docker-ce's dep error"
	exit -1
fi 

if ! type docker >/dev/null 2>&1; then
    curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
	sudo add-apt-repository "deb [arch=amd64] http://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
	sudo apt-get update
	sudo apt-get -y install docker-ce
	if [ $? -ne 0 ]; then
		echo "install docker-ce error"
		exit -1
	fi
	echo 'docker installed'
else
    echo 'docker has been installed'
fi

# add current user to user group
sudo groupadd docker
sudo usermod -aG docker $USER