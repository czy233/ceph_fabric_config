#/bin/bash -e

# # func StoreBlock()
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "StoreBlock" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*commit block \(\[.*\]\) cost: \(.*\), block validation: \(.*\), DoesPvtDataInfoExistInLedger: \(.*\), findPrivateData: \(.*\), CommitWithPvtData: \(.*\)\\n.*/validate\&commit Block \1 cost: \2, block validation: \3, DoesPvtDataInfoExistInLedger: \4, findPrivateData: \5, commit: \6/p' ~/my_fabric.log > ~/my_fabric_val_commit.log


# # func validateTx()
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "validateTx" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*in block \(\[.*\]\) cost: \(.*\), Block validation: \(.*\), VSCC: \(.*\), GetTxCCInstance: \(.*\)\\n.*/validateTX Block \1 cost: \2, block validation: \3, VSCC: \4, CCInstance: \5/p' ~/my_fabric.log > ~/my_fabric_validateTX.log

# # func ValidateTransaction()
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "ValidateTransaction" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), ValidateCommonHeader: \(.*\), CheckSignatureFromCreator: \(.*\), CheckTxID: \(.*\), ValidateEndorserTransaction: \(.*\)\\n.*/ValidateTransaction cost: \1, ValidateCommonHeader: \2, CheckSignatureFromCreator: \3, CheckTxID: \4, ValidateEndorserTransaction: \5/p' ~/my_fabric.log > ~/my_fabric_ValidateTransaction.log

# # func checkSignatureFromCreator()
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "checkSignatureFromCreator" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), GetIdentityDeserializer: \(.*\), DeserializeIdentity: \(.*\), Validate: \(.*\), Verify: \(.*\)\\n.*/ValidateTransaction cost: \1, GetIdentityDeserializer: \2, DeserializeIdentity: \3, Validate: \4, Verify: \5/p' ~/my_fabric.log > ~/my_fabric_checkSignatureFromCreator.log

# # func Verify()
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "Verify " | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), getHashOpt: \(.*\), Hash: \(.*\), Verify: \(.*\)\\n.*/cost: \1, getHashOpt: \2, Hash: \3, Verify: \4/p' ~/my_fabric.log > ~/my_fabric_Verify.log

# # func (csp *CSP) Verify()
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "\[bccsp_sw\] Verify" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\)\\n.*/(csp *CSP) Verify cost: \1/p' ~/my_fabric.log > ~/my_fabric_CSP_Verify.log

# # func (v *ecdsaPublicKeyKeyVerifier) Verify()
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "\[verifier\] Verify" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\)\\n.*/(v *ecdsaPublicKeyKeyVerifier) Verify cost: \1/p' ~/my_fabric.log > ~/my_fabric_Verifier.log


# # func VSCCValidateTx()
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "VSCCValidateTx" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), Unmarshal: \(.*\), V1_2Validation: \(.*\), Namespace: \(.*\), VSCCValidateTxForCC: \(.*\)\\n.*/VSCCValidateTx cost: \1, Unmarshal: \2, V1_2Validation: \3, Namespace: \4, VSCCValidateTxForCC: \5/p' ~/my_fabric.log > ~/my_fabric_VSCCValidateTx.log
# sed -n 's/.*VSCC cost: \(.*\), GetInfoForValidate: \(.*\), VSCCValidateTxForCC: \(.*\)\\n.*/VSCCValidateTx cost: \1, GetInfoForValidate: \2, VSCCValidateTxForCC: \3/p' ~/my_fabric.log > ~/my_fabric_VSCCValidateTx.log

# # func ValidateWithPlugin()
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "ValidateWithPlugin" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), getOrCreatePlugin: \(.*\), Validate: \(.*\)\\n.*/ValidateWithPlugin cost: \1, getOrCreatePlugin: \2, Validate: \3/p' ~/my_fabric.log > ~/my_fabric_ValidateWithPlugin.log

# # func (vscc *Validator) Validate v12
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "deduplicateIdentity" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), Unmarshal: \(.*\), deduplicateIdentity: \(.*\), Evaluate: \(.*\), LSCC: \(.*\)\\n.*/ValidateWithPlugin V12 cost: \1, Unmarshal: \2, deduplicateIdentity: \3, Evaluate: \4, LSCC: \5/p' ~/my_fabric.log > ~/my_fabric_ValidateWithPlugin_V12.log

# # func (id *PolicyEvaluator) Evaluate
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "\[committer.txvalidator\] Evaluate" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), NewPolicy: \(.*\), Evaluate: \(.*\)\\n.*/Evaluate cost: \1, NewPolicy: \2, Evaluate: \3/p' ~/my_fabric.log > ~/my_fabric_Evaluate.log

# # func (p *policy) Evaluate
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "\[cauthdsl\] Evaluate" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), signatureSet: \(.*\), evaluator: \(.*\)\\n.*/Evaluate cost: \1, signatureSet: \2, evaluator: \3/p' ~/my_fabric.log > ~/my_fabric_Evaluate.log

# # func compile
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "compile" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), Identity: \(.*\), SatisfiesPrincipal: \(.*\), Verify: \(.*\)\\n.*/compile cost: \1, Identity: \2, SatisfiesPrincipal: \3, Verify: \4/p' ~/my_fabric.log > ~/my_fabric_compile.log

# # func (vscc *Validator) Validate v13
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "PreValidate" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), PreValidate: \(.*\), extractValidationArtifacts: \(.*\), Validate: \(.*\), LSCC: \(.*\), PostValidate: \(.*\)\\n.*/ValidateWithPlugin Validate cost: \1, PreValidate: \2, extractValidationArtifacts: \3, Validate: \4, LSCC: \5, PostValidate: \6/p' ~/my_fabric.log > ~/my_fabric_ValidateWithPlugin_Validate.log

# # func (klv *KeyLevelValidator) Validate
# sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "checkRWSet" | grep "my logs" > ~/my_fabric.log
# sed -n 's/.*cost: \(.*\), checkRWSet: \(.*\), checkCCEPIfNoEPChecked: \(.*\)\\n.*/ValidateWithPlugin Validate cost: \1, checkRWSet: \2, checkCCEPIfNoEPChecked: \3/p' ~/my_fabric.log > ~/my_fabric_ValidateWithPlugin_Validate.log

# func (l *kvLedger) CommitWithPvtData
sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "state_validation" | grep "500 transaction" > ~/my_fabric.log
sed -n 's/.*Committed block .* in \(.*\)ms (state_validation=\(.*\)ms block_and_pvtdata_commit=\(.*\)ms state_commit=\(.*\)ms).*/\1 \2 \3 \4/p' ~/my_fabric.log > ~/my_fabric_CommitWithPvtData.log
awk '{sum1+=$1;sum2+=$2;sum3+=$3;sum4+=$4}END{print "(l *kvLedger) CommitWithPvtData():",sum1/NR,"ms, state_validation:",sum2/NR,"ms, block_and_pvtdata_commit:",sum3/NR,"ms, state_commit:",sum4/NR,"ms"}' ~/my_fabric_CommitWithPvtData.log

# func (s *Store) CommitWithPvtData
sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "LastCommittedBlockHeight"  > ~/my_fabric.log
sed -n 's/.*cost: \(.*\)μs, LastCommittedBlockHeight: \(.*\)μs, Prepare: \(.*\)μs, AddBlock: \(.*\)μs, Commit: \(.*\)μs\\n.*/\1 \2 \3 \4 \5/p' ~/my_fabric.log > ~/my_fabric_CommitWithPvtData.log
awk '{sum1+=$1;sum2+=$2;sum3+=$3;sum4+=$4;sum5+=$5}END{print "(s *Store) CommitWithPvtData():",sum1/NR,"μs, LastCommittedBlockHeight:",sum2/NR,"μs, Prepare:",sum3/NR,"μs, AddBlock:",sum4/NR,"μs, Commit:",sum5/NR,"μs"}' ~/my_fabric_CommitWithPvtData.log


# func (mgr *blockfileMgr) addBlock
sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "addBlock" | grep "500 transaction" > ~/my_fabric.log
sed -n 's/.*cost: \(.*\)μs, BlockWriting: \(.*\)μs, UpdateIndexDB: \(.*\)μs, updateSubInfo: \(.*\)μs\\n.*/\1 \2 \3 \4/p' ~/my_fabric.log > ~/my_fabric_addBlock.log
awk '{sum1+=$1;sum2+=$2;sum3+=$3;sum4+=$4}END{print "(mgr *blockfileMgr) addBlock:",sum1/NR,"μs, BlockWriting:",sum2/NR,"μs, UpdateIndexDB:",sum3/NR,"μs, updateSubInfo:",sum4/NR,"μs"}' ~/my_fabric_addBlock.log

# func (index *blockIndex) indexBlock
sudo cat `docker inspect --format='{{.LogPath}}' peer0.org1.demo.com` | grep "indexBlock"  > ~/my_fabric.log
sed -n 's/.*cost: \(.*\)μs, Perpare: \(.*\)μs, Index1: \(.*\)μs, Index2: \(.*\)μs, Index3: \(.*\)μs, Index4: \(.*\)μs, Index5: \(.*\)μs, Index6: \(.*\)μs, WriteBatch: \(.*\)μs\\n.*/\1 \2 \3 \4 \5 \6 \7 \8 \9/p' ~/my_fabric.log > ~/my_fabric_indexBlock.log
awk '{sum1+=$1;sum2+=$2;sum3+=$3;sum4+=$4;sum5+=$5;sum6+=$6;sum7+=$7;sum8+=$8;sum9+=$9}END{print "(index *blockIndex) indexBlock():",sum1/NR,"μs, Perpare:",sum2/NR,"μs, Index1:",sum3/NR,"μs, Index2:",sum4/NR,"μs, Index3:",sum5/NR,"μs, Index4:",sum6/NR,"μs, Index5:",sum7/NR,"μs, Index6:",sum8/NR,"μs, WriteBatch:",sum9/NR,"μs"}' ~/my_fabric_indexBlock.log