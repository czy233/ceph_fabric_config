#!/bin/bash -e

source cluster_IP.conf

FABRIC_PATH="$GOPATH/src/github.com/hyperledger/fabric"
FABRIC_BRANCH="C1.4"
FABRIC_SDK_BRANCH="testWithCeph"

# down掉正在运行的区块链系统
cd ~/ceph_fabric_config/fabric_deploy_splitted_raft_1.4
./down_fabric_cluster.sh

# get fabric codes & make docker images
echo "get fabric source & make docker images codes ..."
for node in ${FABRIC_PEER_IP[@]}
do
    ssh $node "
        source /etc/profile
        while [ ! -d "\$FABRIC_PATH" ]; do
            git clone -q https://gitee.com/czy233/fabric.git $FABRIC_PATH
            sleep 10s
        done
        cd $FABRIC_PATH
        git pull
        git checkout $FABRIC_BRANCH
        git pull

        # docker rm \$(docker ps -aq --filter status="exited") > /dev/null 2>&1
        # docker rmi \$(docker images | awk '/^<none>/ { print \$3 }') > /dev/null 2>&1
        make clean > /dev/null 2>&1
        make peer-docker
        make tools-docker
    " &
done
wait

for node in ${FABRIC_ORDERER_IP[@]}
do
    ssh $node "
        source /etc/profile
        while [ ! -d "\$FABRIC_PATH" ]; do
            git clone -q https://gitee.com/czy233/fabric.git $FABRIC_PATH
            sleep 10s
        done
        cd $FABRIC_PATH
        git pull
        git checkout $FABRIC_BRANCH
        git pull

        # docker rm \$(docker ps -aq --filter status="exited") > /dev/null 2>&1
        # docker rmi \$(docker images | awk '/^<none>/ { print \$3 }') > /dev/null 2>&1
        make clean > /dev/null 2>&1
        make orderer-docker
    " &
done
wait
echo "get fabric source codes & make docker images done"

# # run fabric & benchmark
# ssh $CLIENT "
#     source /etc/profile
#     cd \$GOPATH/src/github.com/hyperledger/fabric-sdk/benchmark
#     git pull
#     git checkout $FABRIC_SDK_BRANCH
#     git pull
#     ./redeploy_run_benchmark.sh
# "

# test fabric using ceph
# cd ~/ceph_fabric_config/test_ceph_fabric
# deploy fabric with 1 peer in an org, using a solo orderer
# ./test_ceph_fabric.sh