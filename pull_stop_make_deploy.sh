#!/bin/bash -e

source IP.conf

echo "start deploy a ceph cluster!"
# update ceph code
# stop all ceph process
# build ceph code & install
for node in $CLIENT $NODE1 $NODE2 $NODE3
do
    (
        echo "ssh to [${node}] to update & stop & make"
        ssh ${node} "
            if [ ! -d '/home/$USER/ceph_fabric_config' ]; then
                git clone https://gitee.com/czy233/ceph_fabric_config.git /home/$USER/ceph_fabric_config 
            else
                cd /home/$USER/ceph_fabric_config
                git pull
            fi
            cd /home/$USER/ceph_fabric_config
            ./stop_to_make.sh"
    ) &
done
wait

# deploy a ceph cluster with 1 mon & 3 osds
cd /home/$USER/ceph_fabric_config
./deploy.sh

echo "Complete!"
echo "finish deploy a ceph cluster!"