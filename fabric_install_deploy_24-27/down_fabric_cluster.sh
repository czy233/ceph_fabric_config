#!/bin/bash

source /etc/profile

# down fabric network
echo -n "down fabric network ... "
for i in {0..3}
do
    ssh node$i "
        cd $GOPATH/src/fabric-sdk/gen_crypto_24-27
        docker-compose -f docker-compose-${i}.yaml down -v > /dev/null 2>&1
    " &
done
wait
echo "done"