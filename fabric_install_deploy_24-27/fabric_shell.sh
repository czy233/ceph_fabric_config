#!/bin/bash

# change to work path
cd /home/mcloud/go/src/fabric-sdk/gen_crypto/
export FABRIC_CFG_PATH=${PWD}/config/fabric/
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/crypto-config/peerOrganizations/org1.demo.com/peers/peer0.org1.demo.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org1.demo.com/users/Admin@org1.demo.com/msp
export CORE_PEER_ADDRESS=peer0.org1.demo.com:7051

CHANNEL_NAME='mychannel'
CHAINCODE_NAME='mycc'
ORDERER_CA_FILE="${PWD}/crypto-config/ordererOrganizations/demo.com/orderers/orderer0.demo.com/msp/tlscacerts/tlsca.demo.com-cert.pem"
PEER0_CA_FILE="${PWD}/crypto-config/peerOrganizations/org1.demo.com/peers/peer0.org1.demo.com/tls/ca.crt"
PEER1_CA_FILE="${PWD}/crypto-config/peerOrganizations/org2.demo.com/peers/peer0.org2.demo.com/tls/ca.crt"
PEER2_CA_FILE="${PWD}/crypto-config/peerOrganizations/org3.demo.com/peers/peer0.org3.demo.com/tls/ca.crt"
PEER3_CA_FILE="${PWD}/crypto-config/peerOrganizations/org4.demo.com/peers/peer0.org4.demo.com/tls/ca.crt"

function put() {
    if [ $# -lt 2 ]; then
        printHelp "put"
        exit 0
    fi
    echo "start put an object's hash to fabric"
    set -x
    peer chaincode invoke -o orderer0.demo.com:7050 --ordererTLSHostnameOverride orderer0.demo.com --tls \
    --cafile ${ORDERER_CA_FILE} \
    -C ${CHANNEL_NAME} -n ${CHAINCODE_NAME} \
    --peerAddresses peer0.org1.demo.com:7051 \
    --tlsRootCertFiles ${PEER0_CA_FILE} \
    --peerAddresses peer0.org2.demo.com:7051 \
    --tlsRootCertFiles ${PEER1_CA_FILE} \
    --peerAddresses peer0.org3.demo.com:7051 \
    --tlsRootCertFiles ${PEER2_CA_FILE} \
    --peerAddresses peer0.org4.demo.com:7051 \
    --tlsRootCertFiles ${PEER3_CA_FILE} \
    -c '{"function":"CreateObject","Args":["'$1'", "'$2'"]}'
    res=$?
    { set +x; } 2>/dev/null
    if [ $res -ne 0 ]; then
        exit -1
    fi
    echo "end put an object's hash to fabric"
    exit 0
}

function update() {
    if [ $# -lt 2 ]; then
        printHelp "update"
        exit 0
    fi
    echo "start update an object's hash to fabric"
    set -x
    peer chaincode invoke -o orderer0.demo.com:7050 --ordererTLSHostnameOverride orderer0.demo.com --tls \
    --cafile ${ORDERER_CA_FILE} \
    -C ${CHANNEL_NAME} -n ${CHAINCODE_NAME} \
    --peerAddresses peer0.org1.demo.com:7051 \
    --tlsRootCertFiles ${PEER0_CA_FILE} \
    --peerAddresses peer0.org2.demo.com:7051 \
    --tlsRootCertFiles ${PEER1_CA_FILE} \
    --peerAddresses peer0.org3.demo.com:7051 \
    --tlsRootCertFiles ${PEER2_CA_FILE} \
    --peerAddresses peer0.org4.demo.com:7051 \
    --tlsRootCertFiles ${PEER3_CA_FILE} \
    -c '{"function":"UpdateObject","Args":["'$1'", "'$2'"]}'
    res=$?
    { set +x; } 2>/dev/null
    if [ $res -ne 0 ]; then
        exit -1
    fi
    echo "end update an object's hash to fabric"
    exit 0
}

function delete() {
    if [ $# -lt 1 ]; then
        printHelp "delete"
        exit 0
    fi
    echo "start delete an object's hash from fabric"
    set -x
    peer chaincode invoke -o orderer0.demo.com:7050 --ordererTLSHostnameOverride orderer0.demo.com --tls \
    --cafile ${ORDERER_CA_FILE} \
    -C ${CHANNEL_NAME} -n ${CHAINCODE_NAME} \
    --peerAddresses peer0.org1.demo.com:7051 \
    --tlsRootCertFiles ${PEER0_CA_FILE} \
    --peerAddresses peer0.org2.demo.com:7051 \
    --tlsRootCertFiles ${PEER1_CA_FILE} \
    --peerAddresses peer0.org3.demo.com:7051 \
    --tlsRootCertFiles ${PEER2_CA_FILE} \
    --peerAddresses peer0.org4.demo.com:7051 \
    --tlsRootCertFiles ${PEER3_CA_FILE} \
    -c '{"function":"DeleteObject","Args":["'$1'"]}'
    res=$?
    { set +x; } 2>/dev/null
    if [ $res -ne 0 ]; then
        exit -1
    fi
    echo "end delete an object's hash from fabric"
}

function delete_all() {
    echo "start delete all object's hash from fabric"
    set -x
    peer chaincode invoke -o orderer0.demo.com:7050 --ordererTLSHostnameOverride orderer0.demo.com --tls \
    --cafile ${ORDERER_CA_FILE} \
    -C ${CHANNEL_NAME} -n ${CHAINCODE_NAME} \
    --peerAddresses peer0.org1.demo.com:7051 \
    --tlsRootCertFiles ${PEER0_CA_FILE} \
    --peerAddresses peer0.org2.demo.com:7051 \
    --tlsRootCertFiles ${PEER1_CA_FILE} \
    --peerAddresses peer0.org3.demo.com:7051 \
    --tlsRootCertFiles ${PEER2_CA_FILE} \
    --peerAddresses peer0.org4.demo.com:7051 \
    --tlsRootCertFiles ${PEER3_CA_FILE} \
    -c '{"function":"DeleteAllObjects","Args":[]}'
    res=$?
    { set +x; } 2>/dev/null
    if [ $res -ne 0 ]; then
        exit -1
    fi
    echo "end delete all object's hash from fabric"
}

function query() {
    if [ $# -lt 1 ]; then
        printHelp "query"
        exit 0
    fi
    # echo "start query an object's hash to fabric"
    # set -x
    peer chaincode query -C ${CHANNEL_NAME} -n ${CHAINCODE_NAME} -c '{"Args":["ReadObject", "'$1'"]}'
    # res=$?
    # { set +x; } 2>/dev/null
    if [ $? -ne 0 ]; then
        exit -1
    fi
    # echo "end query an object's hash to fabric"
    exit 0
}

function query_all() {
    echo "start query all object's hash to fabric"
    set -x
    peer chaincode query -C ${CHANNEL_NAME} -n ${CHAINCODE_NAME} -c '{"Args":["GetAllObjects"]}'
    # peer chaincode invoke -o orderer0.demo.com:7050 --ordererTLSHostnameOverride orderer0.demo.com --tls \
    # --cafile ${ORDERER_CA_FILE} \
    # -C ${CHANNEL_NAME} -n ${CHAINCODE_NAME} \
    # --peerAddresses peer0.org1.demo.com:7051 \
    # --tlsRootCertFiles ${PEER0_CA_FILE} \
    # --peerAddresses peer0.org2.demo.com:7051 \
    # --tlsRootCertFiles ${PEER1_CA_FILE} \
    # --peerAddresses peer0.org3.demo.com:7051 \
    # --tlsRootCertFiles ${PEER2_CA_FILE} \
    # --peerAddresses peer0.org4.demo.com:7051 \
    # --tlsRootCertFiles ${PEER3_CA_FILE} \
    # -c '{"function":"GetAllObjects","Args":[]}'
    res=$?
    { set +x; } 2>/dev/null
    if [ $res -ne 0 ]; then
        exit -1
    fi
    echo "end query all object's hash to fabric"
    exit 0
}

function query_example() {
    echo "start query an object's hash to fabric"
    set -x
    peer chaincode query -C ${CHANNEL_NAME} -n ${CHAINCODE_NAME} -c '{"Args":["ReadObject", "object_0"]}'
    res=$?
    { set +x; } 2>/dev/null
    if [ $res -ne 0 ]; then
        exit -1
    fi
    echo "end query an object's hash to fabric"
    exit 0
}

function printHelp() {
    if [ $# -ne 0 ]; then
        USAGE="$1"
    else
        USAGE="NA"
    fi
    if [ "$USAGE" == "put" ]; then
        echo "Usage: "
        echo "  ./fabric_shell.sh put <object_id> <obj_hash>"
        echo "    object_id: unique string described object"
        echo "    obj_hash: hash of the object"
        echo "  Example:"
        echo "    ./fabric_shell.sh put object_0 ec13a4c02aaa0ef53893742d5bf7d14c960333e0275a9c6472774e85c9f502c5"
    elif [ "$USAGE" == "update" ]; then
        echo "Usage: "
        echo "  ./fabric_shell.sh update <object_id> <obj_hash>"
        echo "    object_id: unique string described object"
        echo "    obj_hash: hash of the object"
        echo "  Example:"
        echo "    ./fabric_shell.sh update object_0 ec13a4c02aaa0ef53893742d5bf7d14c960333e0275a9c6472774e85c9f502c5"
    elif [ "$USAGE" == "delete" ]; then
        echo "Usage: "
        echo "  ./fabric_shell.sh delete <object_id>"
        echo "    object_id: unique string described object"
        echo "  Example:"
        echo "    ./fabric_shell.sh delete object_0"
    elif [ "$USAGE" == "delete_all" ]; then
        echo "Usage: "
        echo "  ./fabric_shell.sh delete_all"
        echo "  Example:"
        echo "    ./fabric_shell.sh delete_all"
    elif [ "$USAGE" == "query" ]; then
        echo "Usage: "
        echo "  ./fabric_shell.sh query <object_id>"
        echo "    object_id: unique string described object"
        echo "  Example:"
        echo "    ./fabric_shell.sh query object_0"  
    elif [ "$USAGE" == "query_all" ]; then
        echo "Usage: "
        echo "  ./fabric_shell.sh query_all"
        echo "  Example:"
        echo "    ./fabric_shell.sh query_all"  
    elif [ "$USAGE" == "query_example" ]; then
        echo "Usage: "
        echo "  ./fabric_shell.sh query_example"
        echo "  Example:"
        echo "    ./fabric_shell.sh query_example"
    else
        echo "Fabric_shell.sh Usage:"
        echo "  ./fabric_shell.sh <Mode>"
        echo "    Modes:"
        echo -e "      \033[0;32mput\033[0m: put a new object's hash to fabric"
        echo -e "      \033[0;32mupdate\033[0m: update an object's hash to fabric"
        echo -e "      \033[0;32mdelete\033[0m: delete an object's hash from fabric"
        echo -e "      \033[0;32mquery_example\033[0m: \033[0;31m!an example!\033[0m query object_0 and object_1's hash from fabric"
        echo ""
        echo "    -h - print this message"
        echo "For example: ./fabric_shell.sh query_example"
        echo "Output is the query result:"
        query_example
        exit 0
    fi
}

## Parse mode
if [[ $# -lt 1 ]] ; then
  printHelp
  exit 0
else
  MODE=$1
  shift
fi

## Deal mode
if [ "$MODE" == "put" ]; then
  put $1 $2
elif [ "$MODE" == "update" ]; then
  update $1 $2
elif [ "$MODE" == "delete" ]; then
  delete $1
elif [ "$MODE" == "query" ]; then
  query $1
elif [ "$MODE" == "query_example" ]; then
  query_example
elif [ "$MODE" == "query_all" ]; then
  query_all
elif [ "$MODE" == "delete_all" ]; then
  delete_all
else
  printHelp
fi
exit 0
