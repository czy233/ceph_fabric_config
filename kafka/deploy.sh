#!/bin/bash -e

IP="10.10.9.40"

kafka=`docker images | grep kafka`
if [ 0"$kafka" == "0" ]; then
    docker pull wurstmeister/kafka
fi

zookeeper=`docker images | grep zookeeper`
if [ 0"$zookeeper" == "0" ]; then
    docker pull wurstmeister/zookeeper
fi

libkafka=`whereis librdkafka | grep librdkafka.so`
if [ 0"$libkafka" == "0" ]; then
    cd $HOME
    git clone https://gitee.com/mirrors/librdkafka.git
    cd librdkafka
    ./configure
    make
    sudo make install
    sudo ldconfig
fi

bash start.sh -i $IP