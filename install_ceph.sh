#!/bin/bash -e
wget http://mirrors.ustc.edu.cn/ceph/tarballs/ceph_16.2.4.orig.tar.gz
gzip -d ceph_16.2.4.orig.tar.gz
tar -xvf ceph_16.2.4.orig.tar
mv ceph-16.2.4 ceph
cd ceph
sudo sed -i s/git+https/git/g ./src/pybind/mgr/rook/rook-client-python/requirements.txt
sudo apt-get -y install python3-pip
pip_config_path="$HOME/.pip"
if [ ! -d $pip_config_path ]; then
    mkdir ~/.pip
fi
sudo rm -rf ~/.pip/pip.conf
echo "[global]
index-url = https://mirrors.aliyun.com/pypi/simple/
[install]
trusted-host=mirrors.aliyun.com" >> ~/.pip/pip.conf
./install-deps.sh
ARGS="-DCMAKE_BUILD_TYPE=RelWithDebInfo -DWITH_MANPAGE=OFF -DWITH_BABELTRACE=OFF -DWITH_MGR_DASHBOARD_FRONTEND=OFF" ./do_cmake.sh
cd build
make -j24
if [ $? -ne 0 ]; then
    make -j12
    if [ $? -ne 0 ]; then
        make -j8
    fi
fi
sudo make install -j24