#!/bin/bash

USER='mcloud'

for i in {1..3}
do
    (
        echo "ssh to [node$i] to uninstall & clean ceph"
        ssh node$i "
            cd /home/$USER/ceph-demo/build
            sudo make uninstall -j24
            sudo make clean -j24
            cd ..
            sudo rm -rf build
            "
    ) &
done
echo "ssh to [client] to uninstall & clean ceph"
ssh client "
    cd /home/$USER/ceph-demo/build
    sudo make uninstall -j24
    sudo make clean -j24
    cd ..
    sudo rm -rf build
    " &
echo "wait uninstall & clean ceph finish ..."
wait

for i in {1..3}
do
    (
        echo "ssh to [node$i] to do cmake"
        ssh node$i "
            cd /home/$USER/ceph-demo
            git pull origin master
            ARGS='-DCMAKE_BUILD_TYPE=RelWithDebInfo -DWITH_MANPAGE=OFF -DWITH_BABELTRACE=OFF -DWITH_MGR_DASHBOARD_FRONTEND=OFF' ./do_cmake.sh
            "
    ) &
done
echo "ssh to [client] to do cmake"
ssh client "
    cd /home/$USER/ceph-demo
    git pull origin master
    ARGS='-DCMAKE_BUILD_TYPE=RelWithDebInfo -DWITH_MANPAGE=OFF -DWITH_BABELTRACE=OFF -DWITH_MGR_DASHBOARD_FRONTEND=OFF' ./do_cmake.sh
    " &
echo "wait do cmake finish ..."
wait
