#!/bin/bash

CEPH_DIR="/home/$USER/ceph-demo"

# stop all ceph process
echo "[$(hostname -s)] stopping ceph cluster..."
ceph_id=`ps -ef | grep ceph- | grep -v "grep" | awk '{print $2}'`
for id in $ceph_id
do
    sudo kill -9 $id  
    echo "[$(hostname -s)] killed $id"  
done
ceph_id=`ps -ef | grep radosgw | grep -v "grep" | awk '{print $2}'`
for id in $ceph_id
do
    sudo kill -9 $id  
    echo "[$(hostname -s)] killed $id"  
done

# update ceph code from gitee
cd $CEPH_DIR
git pull

# rm file of ceph
sudo umount /var/lib/ceph/osd/*
sudo rm -rf /var/lib/ceph/
sudo rm -rf /var/log/ceph/*
sudo rm -rf /var/log/radosgw/*
sudo rm -rf /tmp/ceph*
sudo rm -rf /etc/ceph/*
sudo rm -rf /var/run/ceph/*
sudo rm -rf /tmp/monmap
echo "[$(hostname -s)] stop ceph cluster done!"

# make & install ceph
cd $CEPH_DIR/build

echo "[$(hostname -s)] make ceph in $(hostname -s)"
sudo make -j12 > $CEPH_DIR/build/make_ceph.log 2>&1
if [ $? -ne 0 ]; then
    echo "[$(hostname -s)] make ceph error, exited."
    exit -1
fi

echo "[$(hostname -s)] install ceph to $(hostname -s)"
sudo make install -j24 > $CEPH_DIR/build/install_ceph.log 2>&1
if [ $? -ne 0 ]; then
    echo "[$(hostname -s)] install ceph error, exited."
    exit -1
fi
echo "[$(hostname -s)] finish install ceph to $(hostname -s)"