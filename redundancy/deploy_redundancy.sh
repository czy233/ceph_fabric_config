#/bin/bash -e

source IP.conf

FABRIC_SDK_BRANCH="testWithCeph"
REDUNDANCY_BRANCH="unionTest"

i=0

./down_redundancy.sh

for node in ${CEPH_IP[@]}
do
    ssh $node "
        source /etc/profile
        if [ ! -d "\$GOPATH/src/github.com/hyperledger/fabric-sdk" ]; then
            mkdir -p \$GOPATH/src/github.com/hyperledger
            git clone https://gitee.com/czy233/fabric-sdk.git \$GOPATH/src/github.com/hyperledger/fabric-sdk
            cd \$GOPATH/src/github.com/hyperledger/fabric-sdk
            git checkout $FABRIC_SDK_BRANCH
        else
            cd \$GOPATH/src/github.com/hyperledger/fabric-sdk
            git pull
            git checkout $FABRIC_SDK_BRANCH
            git pull
        fi
        if [ ! -d "\$HOME/redundancy" ]; then
            git clone https://gitee.com/zhujerry/redundancy.git \$HOME/redundancy
            cd \$HOME/redundancy/src
            git checkout $REDUNDANCY_BRANCH
            git pull
        else
            cd \$HOME/redundancy/src
            git pull
            git checkout $REDUNDANCY_BRANCH
            git pull
        fi
        ./hosts_config.sh
        go run . -c $i > $HOME/redundancy.log
    " &
    let i=$i+1
done 
wait