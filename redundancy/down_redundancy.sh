#/bin/bash -e

source IP.conf

i=0

for node in ${CEPH_IP[@]}
do
    ssh $node "
        source /etc/profile
        fabric_sdk=\$(ps -ef | grep 'exe/src -c' | grep -v grep | awk '{print \$2}')
        for id in \$fabric_sdk
        do
            sudo kill -9 \$id
        done

        fabric_sdk=\$(ps -ef | grep 'go run . -c' | grep -v grep | awk '{print \$2}')
        for id in \$fabric_sdk
        do
            sudo kill -9 \$id
        done
    " &
    let i=$i+1
done 
wait