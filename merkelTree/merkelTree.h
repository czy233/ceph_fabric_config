#include <cstdio>
#include <string>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <openssl/sha.h> // add opensll/sha to support sha256
#include <openssl/evp.h>
#include <chrono>
#include <thread>
#include <vector>
// #include "boost/bind/bind.hpp"
// #include "boost/threadpool.hpp"
// #include "boost/asio.hpp"
// #include "boost/asio/thread_pool.hpp"

// struct leaf
// {
//     std::string leaf_hash;
// };

// struct node16
// {
//     std::string node_hash;
//     leaf *leafs[16];
// };

// struct node4
// {
//     std::string node_hash;
//     node16 *node16s[16];
// };

// struct merkelTree
// {
//     std::string root;
//     node4 *node4s[4];
// };

struct node
{
    std::string node_hash;
    std::string leaf_hashs[32];
};

struct merkelTree
{
    std::string root;
    node *nodes[32];
};

// void calc_hash_of_data_with_sha256(const std::string &data, std::string &hash);
void calc_hash_of_data_with_sha256(const char *data, int length, std::string &res_hash);

void single_thread_cal_merkel_hash(std::string &content, std::string &hash);
void muti_thread_cal_merkel_hash(std::string &content, std::string &hash);
void cal_sub_merkel_hash(std::string &content, int num, node **sub_root);

void print_hash(std::string &hash);